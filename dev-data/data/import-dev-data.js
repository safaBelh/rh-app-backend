// Terminal COMMANDS
// node dev-data\data\import-dev-data.js --import
// node dev-data\data\import-dev-data.js --delete

require("dotenv").config();
let fs = require("fs");
let mongoose = require("mongoose");
let connectingString = process.env.DATABASE.replace(
  "<PASSWORD>",
  process.env.DATABASE_PASSWORD
);

// Importing Models
let User = require("../../models/userModel");
let Project = require("../../models/projectModel");
let Task = require("../../models/taskModel");

let users = JSON.parse(fs.readFileSync(`${__dirname}/users.json`, "utf-8"));
let projects = JSON.parse(
  fs.readFileSync(`${__dirname}/projects.json`, "utf-8")
);
let tasks = JSON.parse(fs.readFileSync(`${__dirname}/tasks.json`, "utf-8"));

mongoose.connect(connectingString, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: true,
  useUnifiedTopology: true,
});
console.log("DB Connecting ... ");

// Import Data Into DB
let importData = async () => {
  try {
    await User.create(users, { validateBeforeSave: false });
    await Task.create(tasks);
    await Project.create(projects);
    console.log("📥 Data Successfully Loaded !");
    process.exit();
  } catch (error) {
    console.log("Error");
  }
};

// Delete All Data From DB
let DeleteData = async () => {
  try {
    await User.deleteMany();
    await Task.deleteMany();
    await Project.deleteMany();
    console.log(" 🗑️  Data Successfully Deleted !");
    process.exit();
  } catch (error) {
    console.log(error);
  }
};

if (process.argv[2] === "--import") {
  importData();
} else if (process.argv[2] === "--delete") {
  DeleteData();
}
