const express = require("express");
const app = express();
const cors = require("cors");

const AppError = require("./utils/appError");
const globalErrorHandler = require("./controllers/errorController");

// let userRouter = require("./routes/userRoute");
let jobRouter = require("./routes/jobRoute.js");

app.use(express.json());
app.use(cors());

//////////////////  SECURITY
// const rateLimit = require("express-rate-limit");
// const helmet = require("helmet");
// const mongoSanitize = require("express-mongo-sanitize");
// const xss = require("xss-clean");
// const hpp = require("hpp");
//////////////////  SECURITY

//////////////////   SECURITY

// Global MiddleWares

// Set Security HTTP Headers
// app.use(helmet());
// // development Logging
// // Limit Requests From Same API
// limiter = rateLimit({
//   max: 100,
//   windowMs: 60 * 60 * 1000,
//   message: "Too Many Requests From This IP , Please Try Again In An Hour ! ",
// });
// app.use("/api", limiter);

// // Body Parser , Reading Data From Body Into req.body
// app.use(express.json({ limit: "10kb" }));
// // Data sanitization against NoSQL query injection
// app.use(mongoSanitize());
// // Data sanitization against XSS
// app.use(xss());
// // Prevent Parameter Pollution
// app.use(
//   hpp({
//     whiteList: [
//       "duration",
//       "price",
//       "ratingsAverage",
//       "maxGroupSize",
//       "difficulty",
//       "ratingsQuantity",
//     ],
//   })
// );
// //  Serving Static Files
// app.use(express.static(`${__dirname}/public`));

//////////////////  SECURITY

// ROUTES

//  Routes
app.use("/api/v1/jobs", jobRouter);

// Error Handler
app.use(globalErrorHandler);

// Handling Unhandled Routes
app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server ! `, 404));
});

// Exporting APP
module.exports = app;
